#!/usr/bin/env python

import argparse
import sys
import subprocess
import os

this_directory = os.path.dirname(os.path.realpath(__file__))


parser = argparse.ArgumentParser(
    description='Submit requests for interactive jobs. ',
    usage='{} --help'.format(sys.argv[0]))


parser.add_argument('-d', '--device', type=str, choices=['cpu', 'gpu', 'cpuamd', 'gput4', 'gpuv100', 'gpua100'],
                    default='cpu',
                    help='Specify the device you want to access.')

parser.add_argument('-t', '--time', type=int, default=3600, 
                    help='Max duration of your session in seconds.')

parser.add_argument('-c', '--cores', type=int, default=4, 
                    help='Number of cores of your request.')

parser.add_argument('-m', '--memory', type=int, default=8, 
                    help='RAM memory size of your request in GB.')


def main():
    device_sub_dict = {
        'cpu': f'{this_directory}/cpu-any.sub',
        'gpu': f'{this_directory}/gpu-any.sub',
        'cpuamd': f'{this_directory}/cpu-amd.sub',
        'gput4': f'{this_directory}/gpu-t4.sub',
        'gpuv100': f'{this_directory}/gpu-v100.sub',
        'gpua100': f'{this_directory}/gpu-a100.sub',
    }

    # device_requirements_dict = {
    #     'cpu': '',
    #     'cpuamd': '-a requirements=\'(CpuFamily =?= 23)\'',
    #     'gpu': '-a request_gpus=1',
    #     'gput4': '-a request_gpus=1 -a requirements=\'TARGET.CUDADeviceName =?= "Tesla T4" && (TARGET.OpSysAndVer =?= "AlmaLinux9" || TARGET.OpSysAndVer =?= "CentOS7")\'',
    #     'gpuv100': '-a request_gpus=1 -a requirements=\'regexp("V100", TARGET.CUDADeviceName) && (TARGET.OpSysAndVer =?= "AlmaLinux9" || TARGET.OpSysAndVer =?= "CentOS7")\'',
    #     'gpua100': '-a request_gpus=1 -a requirements=\'regexp("A100", TARGET.CUDADeviceName) && (TARGET.OpSysAndVer =?= "AlmaLinux9" || TARGET.OpSysAndVer =?= "CentOS7")\'',
    # }

    args = parser.parse_args()

    submit_file = device_sub_dict[args.device]
    # submit_file = f'{this_directory}/base.sub'

    submit_options = ['condor_submit -interactive',
                     f'-a +MaxRuntime={args.time}', 
                     f'-a request_cpus={args.cores}', 
                     f'-a request_memory={args.memory}G',
                    #  f'{device_requirements_dict[args.device]}',
                     f'-file {submit_file}']
    
    submit_options = ' '.join(submit_options)
    print('Submit command:')
    print(submit_options)
    subprocess.run(submit_options, shell=True)


if __name__ == '__main__':
    main()