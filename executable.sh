#!/usr/bin/env bash

# Define the following variables either here or in your bashrc
BLONDHOME="/path/to/BLonD"
PYTHONHOME="/path/to/python/bin/"
SIMHOME="/path/to/blond-submission-scripts"
MAINFILE=$SIMHOME/mainfile.py
export PYTHONPATH="$BLONDHOME:$PYTHONPATH"
export PATH="$PYTHONHOME:$PATH"

# Alternatively, some of them can be defined in your bashrc
# source $HOME/.bashrc

# The first (and only) argument passed to this script is the simulation directory
SIMDIR=$1

# change directory
cd $SIMDIR

# run main file inside simulation directory, pass input.yml configuration
python3 $MAINFILE --config $SIMDIR/input.yml
